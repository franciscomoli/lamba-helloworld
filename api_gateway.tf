resource "aws_api_gateway_rest_api" "example" {
  name        = "gateway_${var.lambda_function_name}"
  description = "Terraform Serverless Application Example"
  tags = var.tags
}
output "base_url" {
  value = aws_api_gateway_deployment.example.invoke_url
}

output "x-api-key" {
  value = aws_api_gateway_api_key.mykey.value
  sensitive = true
}