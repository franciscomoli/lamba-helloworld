
variable "lambda_function_name" {
  default = "hellomili"
}
variable "iam_role" {
  default = "Lambda-AssumeRole"
}

variable "tags" {
  default = {
    Name        = "prueba"
  }
}

variable vpc_subnet_ids{
  type = list(string)
  default = ["subnet-03f99413a48844f9e","subnet-00afac7d90bc74a1d"]
}

variable vpc_security_group_ids{
  type = list(string)
  default = ["sg-079e3cc3d4764ec64"]

}

variable policy {
  type = list(string)
  default = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AmazonVPCFullAccess",
    "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole",
  ]
}


